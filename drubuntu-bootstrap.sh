#!/usr/bin/env bash
#
# Bootstrap drubuntu.
#

CORE_VERSION='6.x'
DEFAULT_WORKSPACE=$HOME/workspace

echo
echo 'This script bootstraps Drubuntu, by ensuring PHP, CVS, the latest Drush,'
echo 'and Drubuntu projects are installed and available.'
echo 'Once this is complete you will be able to install the drubuntu environment.'

echo
read -n1 -p "Would you like to bootstrap the Drubuntu environment now? (y/n)"
echo

PACKAGES="cvs php5-cli"
echo "Please enter your password to install $PACKAGES."
sudo apt-get -y install $PACKAGES

# Create default directory structure
mkdir -p $DEFAULT_WORKSPACE
mkdir -p $HOME/.drubuntu/backups

# Save a short php script to parse update XML.
echo '<?php $xml = simplexml_load_file("/tmp/drush-release-history.xml");        
$recommended_major = $xml->xpath("/project/recommended_major");                                                                                                                                                               $recommended_major = $xml->xpath("/project/recommended_major"); 
$releases = $xml->xpath("/project/releases/release[status=\"published\"][version_major=" . (string)$recommended_major[0] . "]"); 
$release = (array)$releases[0];
print($release["tag"]);' > /tmp/package_url.php

# Download the XML for drush releases
wget -q -m --timeout=30 -O/tmp/drush-release-history.xml "http://updates.drupal.org/release-history/drush/$CORE_VERSION"
# Download drush (we use cvs to encourage contribution)
cd $DEFAULT_WORKSPACE
if [ -d "$HOME/workspace/drush" ] ; then
  echo 'Backing up existing drush directory'
  mv drush $HOME/.drubuntu/backups/`date +%Y%m%d%H%M%s`-drush.backup
fi

# Temporary pull from Drush HEAD pre-launch.
cvs -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -d drush contributions/modules/drush/
#cvs -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -d drush -r $(php /tmp/package_url.php) contributions/modules/drush/

rm /tmp/drush-release-history.xml /tmp/package_url.php

# Setup .profile
if grep -q drush ~/.profile 
  then
    echo 'Drush appears to already be added to your ~/.profile.'
    echo 'Please check this file and ensure PATH includes $HOME/workspace/drush.'
  else
    echo '
# Set PATH so it includes drush if it exists.
if [ -d "$HOME/workspace/drush" ] ; then
  PATH="$HOME/workspace/drush:$PATH"
fi' >> ~/.profile
  UPDATE_PROFILE=1
fi
PATH="$HOME/workspace/drush:$PATH"
mkdir -p ~/.drush
if [ -d "$HOME/workspace/drubuntu" ] ; then
  echo 'Backing up existing drubuntu directory'
  mv drubuntu $HOME/.drubuntu/backups/`date +%Y%m%d%H%M%s`-drubuntu.backup
fi
drush dl --package-handler=cvs drubuntu --destination=.
ln -sfT $DEFAULT_WORKSPACE/drubuntu ~/.drush/drubuntu

echo
read -n1 -p "Would you like to install the full Drubuntu environment now? (y/n)"
echo
if [ "$REPLY" = "y" ]; then
  drush drubuntu-install
fi

if [ "$UPDATE_PROFILE" = 1 ] ; then
  echo '
To use drush and drubuntu now, update your path by reloading your .profile.
Log in and out of your window manager, or just run the command below for now:

. ~/.profile
'
fi

echo 'Drubuntu bootstrap complete!'
