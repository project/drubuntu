<?php

/**
 * Firefox extension directory.
 * The string in curly braces is the Firefox application ID (this doesn't change).
 */
define('DRUSH_DRUBUNTU_FIREFOX_EXTENSION_DIR', '/.mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/');

class drubuntu_engine_general implements drubuntu_engine {
  function install() {
    // Install core software.
    drush_drubuntu_exec('Adding Google Chrome repository.', "echo 'deb http://dl.google.com/linux/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list && sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A040830F7FAC5991 && sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update", array(), 'Google Chrome repository added successfull.', 'DRUBUNTU_CHROME_REPO_FAILED', 'Problems were encountered adding the Google Chrome repository.');
    drush_drubuntu_exec('Adding Opera repository.', "echo 'deb http://deb.opera.com/opera/ stable non-free' | sudo tee /etc/apt/sources.list.d/opera.list && wget -O - http://deb.opera.com/archive.key | sudo apt-key add - && sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update", array(), 'Opera repository added successfull.', 'DRUBUNTU_OPERA_REPO_FAILED', 'Problems were encountered adding the Opera repository.');
    drush_drubuntu_exec('Installing web development packages.', 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('webdev-general-packages', DRUSH_DRUBUNTU_WEBDEV_GENERAL_PACKAGES), array(), 'Web development packages were installed successfull.', 'DRUBUNTU_LAMP_INSTALL_FAILED', 'Problems were encountered installing web development packages.');
    drush_drubuntu_exec('Symlinking phpmyadmin to workspace (http://phpmyadmin.'. drush_get_option('hostname', 'localhost') .'/).', 'ln -sfT /usr/share/phpmyadmin ' . $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace') .'/phpmyadmin', array(), 'Phpmyadmin symlinked to workspace.', 'DRUBUNTU_PHPMYADMIN_SYMLINK_FAILED', 'Problems were encountered symlinking phpmyadmin to workspace.');
  
    // TODO: IE* using winetricks
  
    // Install Firefox web development Add-ons.
    $packages = explode(',', drush_get_option('firefox-addons', DRUSH_DRUBUNTU_FIREFOX_ADDONS));
    $uris = array();
    foreach ($packages as $package) {
      $uris[] = sprintf('https://addons.mozilla.org/en-US/firefox/downloads/latest/%1$d/addon-%1$d-latest.xpi', $package);
    }
    drush_drubuntu_exec('Installing Firefox web development Add-ons.', "wget -q -nc -c --timeout=30 --directory-prefix='" . $_SERVER['HOME'] . DRUSH_DRUBUNTU_FIREFOX_EXTENSION_DIR . "' '" . implode("' '", $uris) . "'", array(), 'Firefox web development Add-ons installed.', 'DRUBUNTU_FIREFOX_ADDONS_INSTALL_FAILED', 'Problems were encountered installing Firefox web development Add-ons.');
    // Chrome likes to steal the default browser, so we set it back to Firefox here.
    drush_drubuntu_exec('Setting default browser back to Firefox.', 'sudo update-alternatives --set gnome-www-browser /usr/bin/firefox', array(), 'Default browser set back to Firefox.', 'DRUBUNTU_DEFAULT_BROWSER_SET', 'Problems were encountered setting default browser back to Firefox.');
    drush_drubuntu_exec('Setting default browser back to Firefox.', 'sudo update-alternatives --set x-www-browser /usr/bin/firefox', array(), 'Default browser set back to Firefox.', 'DRUBUNTU_DEFAULT_BROWSER_SET', 'Problems were encountered setting default browser back to Firefox.');

    // Download development modules into place.
    $development_modules_location = $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace') . '/' . drush_get_option('development-modules-location', 'development-modules');
    for ($core = 6; $core <= 7; $core++) {
      $project_sets = array(
        1 => DRUSH_DRUBUNTU_DEVELOPMENT_MODULES_P1,
        2 => DRUSH_DRUBUNTU_DEVELOPMENT_MODULES_P2,
        3 => DRUSH_DRUBUNTU_DEVELOPMENT_MODULES_P3,
      );
      foreach ($project_sets as $priority => $set) {
        // Modules are organized by core and priority.
        $destination = $development_modules_location . '/' . $core . '/' . $priority;
        drush_mkdir($destination);
        $projects = explode(',', drush_get_option('development-projects-' . $core . '-p' . $priority, $set));
        foreach ($projects as $id => $project) {
          $projects[$id] = $project . '-' . $core . '.x';
        }
        drush_invoke_process_args('pm-download', $projects, array('destination' => $destination));
      }
    }
  }

  function uninstall($op) {
    drush_drubuntu_exec('Uninstalling web development packages.', 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq ' . $op . ' ' . drush_get_option('webdev-general-packages', DRUSH_DRUBUNTU_WEBDEV_GENERAL_PACKAGES), array(), 'Web development packages were uninstalled successfull.', 'DRUBUNTU_LAMP_UNINSTALL_FAILED', 'Problems were encountered uninstalling web development packages.');
    if ($op == 'purge') {
      drush_drubuntu_exec('Cleaning up files.', 'sudo mv ' . $_SERVER['HOME'] . '/' . drush_get_option('workspace', 'workspace') . '/phpmyadmin ' . drush_drubuntu_backup_dir(), array(), 'Files cleaned up.', 'DRUBUNTU_FILE_CLEANUP_FAILED', 'Problems were encountered cleaning up files.');
    }
  }
  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}
