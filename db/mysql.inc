<?php

class drubuntu_engine_mysql implements drubuntu_engine {
  function install() {
    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('mysql-packages', DRUSH_DRUBUNTU_MYSQL_PACKAGES);
    drush_drubuntu_exec('Installing MySQL packages.', $cmd, array(), 'MySQL install successfull.', 'DRUBUNTU_MYSQL_INSTALL_FAILED', 'Problems were encountered installing MySQL.');
    
    // Add my.cnf if it doesn't exist.
    if (!file_exists($_SERVER['HOME'] . '/.my.cnf')) {
      $my_cnf = "[client]\n";
      foreach (array('user' => 'root', 'password' => '', 'host' => '', 'port' => '') as $parameter => $value) {
        if (!empty($value)) {
          $my_cnf .= $parameter . '=' . drush_get_option('db-' . $parameter, $value) . "\n";
        } 
      }
      file_put_contents($_SERVER['HOME'] . '/.my.cnf', $my_cnf);
    }
  }

  function uninstall($op) {
    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq ' . $op . ' ' . drush_get_option('mysql-packages', DRUSH_DRUBUNTU_MYSQL_PACKAGES);
    drush_drubuntu_exec('Uninstalling MySQL packages.', $cmd, array(), 'MySQL uninstall successfull.', 'DRUBUNTU_MYSQL_UNINSTALL_FAILED', 'Problems were encountered uninstalling MySQL software.');
    if ($op == 'purge') {
      drush_drubuntu_exec('Cleaning up files.', 'sudo mv ' . $_SERVER['HOME'] . '/.my.cnf ' . drush_drubuntu_backup_dir(), array(), 'Files cleaned up.', 'DRUBUNTU_FILE_CLEANUP_FAILED', 'Problems were encountered cleaning up files.');
    }
  }
  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}