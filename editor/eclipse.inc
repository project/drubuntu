<?php

/**
 * EGit Eclipse Plugin URI and packages.
 */
define('DRUSH_DRUBUNTU_ECLIPSE_EGIT_URI', 'http://download.eclipse.org/egit/updates');
define('DRUSH_DRUBUNTU_ECLIPSE_EGIT_PACKAGES', 'org.eclipse.egit,org.eclipse.egit.core,org.eclipse.egit.ui,org.eclipse.egit.doc');

/**
 * Eclipse Drupal plugin URI and packages.
 */
define('DRUSH_DRUBUNTU_ECLIPSE_DRUPAL_URI', 'http://xtnd.us/downloads/eclipse');
define('DRUSH_DRUBUNTU_ECLIPSE_DRUPAL_PACKAGES', 'Drupal_for_Eclipse_PDT.feature.group');

class drubuntu_engine_eclipse implements drubuntu_engine {
  function install() {
    drush_drubuntu_exec('Adding apt-get repository for Eclipse plugins.',  'sudo add-apt-repository ppa:yogarine/eclipse/ubuntu && sudo apt-get update', array(), 'Repository added successfully.', 'DRUBUNTU_ECLIPSE_REPO_FAILED', 'Problems adding the repository.');
    drush_drubuntu_exec('Installing Eclipse and plugins.',  'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('eclipse-packages', DRUSH_DRUBUNTU_ECLIPSE_PACKAGES), array(), 'Eclipse and plugins installed successfully.', 'DRUBUNTU_ECLIPSE_INSTALL_FAILED', 'Problems were encountered installing Eclipse and plugins.');
  
    drush_print('Installing Eclipse configuration.');
    if (is_dir($_SERVER['HOME'] . '/.eclipse')) {
      // Move any existing .eclipse directory out of the way, since we move things
      // around quite a bit.
      drush_op('rename', $_SERVER['HOME'] . '/.eclipse', drush_drubuntu_backup_dir() . '/.eclipse');
    }
    // Increase Eclipse heap to allow at least largish projects without crashing.
    // eclipse.ini will be moved to /etc (a much more sensible place) in the next
    // version of Ubuntu, so while editing it here is nasty, it is not for long.
    $eclipse_ini = file_get_contents('/usr/lib/eclipse/eclipse.ini');
    $eclipse_ini = str_replace('-Xms40m', '-Xms' . drush_get_option('eclipse-xms', '128') . 'M', $eclipse_ini);
    $eclipse_ini = str_replace('-Xmx256m', '-Xmx' . drush_get_option('eclipse-xmx', '512') . 'M', $eclipse_ini);
    drush_drubuntu_sudo_write('/usr/lib/eclipse/eclipse.ini', $eclipse_ini);
    $workspace = $_SERVER['HOME'] . '/' . drush_get_option('workspace', 'workspace');

    // We need to start Eclipse to install these plugins, which won't
    // work if it is already running, so we check here.
    if (!drush_shell_exec('pgrep eclipse') || drush_confirm("Eclipse is currently running, which prevents us from installing plugins.\n- Please exit Eclipse now then press 'y' to continue.\n- If you press 'n' Eclipse plugin install will be skipped and you will need to rerun the install:")) {
      // Install EGit.
      drush_drubuntu_exec('Installing EGit (Eclipse Git support).',  'eclipse -nosplash -data ' . $workspace . ' -application org.eclipse.equinox.p2.director -repository ' . DRUSH_DRUBUNTU_ECLIPSE_EGIT_URI . ' -i ' . DRUSH_DRUBUNTU_ECLIPSE_EGIT_PACKAGES, array(), 'EGit install successfull.', 'DRUBUNTU_EGIT_INSTALL_FAILED', 'Problems were encountered installing EGit.');
    
      // Install Eclipse Drupal plugin.
      drush_drubuntu_exec('Installing Eclipse Drupal plugin.',  'eclipse -nosplash -data ' . $workspace . ' -application org.eclipse.equinox.p2.director -repository ' . DRUSH_DRUBUNTU_ECLIPSE_DRUPAL_URI . ' -i ' . DRUSH_DRUBUNTU_ECLIPSE_DRUPAL_PACKAGES, array(), 'Eclipse Drupal plugin install successfull.', 'DRUBUNTU_DRUPAL_ECLIPSE_PLUGIN_INSTALL_FAILED', 'Problems were encountered installing Eclipse Drupal plugin.');
    }

    // We specify these preferences here, as the Drupal Eclipse plugin is not exhaustive.
    drush_mkdir($workspace . '/.metadata/.plugins/org.eclipse.core.runtime/.settings/');
    $twospaces = array(
      'indentationChar=space',
      'indentationSize=2',
    );
    $files_configs = array(
      'org.eclipse.core.runtime.prefs' => array(
        // Set content type associations.
        'content-types/org.eclipse.php.core.phpsource/file-extensions=module,engine,theme,profile,install,test',
        'org.eclipse.core.runtime.prefs:content-types/org.eclipse.wst.html.core.htmlsource/file-extensions=xtmpl',
      ),
      'org.eclipse.php.core.prefs' => array(
        // Two spaces instead of tabs.
        'org.eclipse.php.core.phpForamtterUseTabs=false',
        'org.eclipse.php.core.phpForamtterIndentationSize=2',
        // Xdebug configuration.
        'org.eclipse.php.debug.coreinstalledPHPInis=/etc/php5/apache2/php.ini',
        'org.eclipse.php.debug.coreinstalledPHPDebuggers=org.eclipse.php.debug.core.xdebugDebugger',
        'org.eclipse.php.debug.coreinstalledPHPDefaults=org.eclipse.php.debug.core.xdebugDebugger\=PHP',
        'org.eclipse.php.debug.coreinstalledPHPLocations=/usr/bin/php',
        'org.eclipse.php.debug.coreinstalledPHPNames=PHP',
        'org.eclipse.php.debug.corephp_debugger_id=org.eclipse.php.debug.core.xdebugDebugger',
      ),
      'org.eclipse.php.ui.prefs' => array(
        // Add "Id" for new files.
        // TODO: Incorporate hook templates here (store as separate XML file and quote).
        'org.eclipse.php.ui.editor.templates=<?xml version\="1.0" encoding\="UTF-8" standalone\="no"?><templates><template autoinsert\="false" context\="newPhp" deleted\="false" description\="Simple php file" enabled\="true" id\="org.eclipse.php.ui.editor.templates.newPhp.author" name\="New simple PHP file">&lt;?php\n// $$' . /* Break to prevent the CVS tag getting expanded by Drupal CVS */ 'Id$$\n${cursor}</template></templates>',
      ),
      'org.eclipse.ui.editors.prefs' => array(
        // Two spaces instead of tabs.
        'spacesForTabs=true',
        'tabWidth=2',
        'printMargin=true',
      ),
      // Two spaces instead of tabs.
      'org.eclipse.wst.css.core.prefs' => $twospaces,
      'org.eclipse.wst.css.ui.prefs' => array(
        // Add "Id" for new files.
        'org.eclipse.wst.sse.ui.custom_templates=<?xml version\="1.0" encoding\="UTF-8" standalone\="no"?><templates><template autoinsert\="true" context\="css_new" deleted\="false" description\="new css file" enabled\="true" id\="org.eclipse.wst.css.ui.internal.templates.newcss" name\="New CSS File">/* $$' . /* Break to prevent the CVS tag getting expanded by Drupal CVS */ 'Id$$ */\n@CHARSET "${encoding}";</template></templates>',
      ),
      'org.eclipse.wst.html.ui.prefs' => array(
        // Disable autoactivate completion (slow), use ctrl-return instead.
        'autoPropose=false',
      ),
      // Two spaces instead of tabs.
      'org.eclipse.wst.html.core.prefs' => $twospaces,
      'org.eclipse.wst.jsdt.core.prefs' => array(
        // Two spaces instead of tabs.
        'org.eclipse.wst.jsdt.core.formatter.tabulation.size=2',
        'org.eclipse.wst.jsdt.core.formatter.indentation.size=2',
        'org.eclipse.wst.jsdt.core.formatter.tabulation.char=space',
      ),
      'org.eclipse.wst.jsdt.ui.prefs' => array(
        // Add "Id" for new files.
        'org.eclipse.wst.jsdt.ui.text.custom_code_templates=<?xml version\="1.0" encoding\="UTF-8" standalone\="no"?><templates><template autoinsert\="false" context\="newtype_context" deleted\="false" description\="Newly created files" enabled\="true" id\="org.eclipse.wst.jsdt.ui.text.codetemplates.newtype" name\="newtype">// $$' . /* Break to prevent the CVS tag getting expanded by Drupal CVS */ 'Id$$\n${filecomment}\n${package_declaration}\n\n${typecomment}\n${type_declaration}</template></templates>',
        // Disable autoactivate completion (slow), use ctrl-return instead.
        'content_assist_autoactivation=false',
      ),
      // Two spaces instead of tabs.
      'org.eclipse.wst.xml.core.prefs' => $twospaces,
    );
    // This takes the above array and merges it with the existing configuration,
    // or adds it if there is no existing configuration.
    foreach ($files_configs as $filename => $configs) {
      $filename = $workspace . '/.metadata/.plugins/org.eclipse.core.runtime/.settings/' . $filename;
      $file = '';
      if (file_exists($filename)) {
        $file = file_get_contents($filename);
      }
      foreach ($configs as $config) {
        $key = substr($config, 0, strpos($config, '='));
        $file = preg_replace('/' . preg_quote($key, '/') . '.*/', $config, $file, -1 , $count);
        if ($count == 0) {
         $file .= $config . "\n";
        }
      }
      file_put_contents($filename, $file);
    }
  
    // Add Drush and Drubuntu projects (patches welcome...hint, hint).
    drubuntu_eclipse_add_project(array($workspace . '/drush', $workspace . '/drubuntu'));
  
    // Copy icon to Desktop, since this is a key application.
    drush_op('copy', '/usr/share/applications/eclipse.desktop', $_SERVER['HOME'] . '/Desktop/eclipse.desktop');
    drush_op('chmod', $_SERVER['HOME'] . '/Desktop/eclipse.desktop', 0755);
  
    drush_log(dt('Eclipse configuration installed.'), 'success');
  }

  function uninstall($op) {
    drush_drubuntu_exec('Uninstalling Eclipse and plugins.',  'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq ' . $op . ' ' . DRUSH_DRUBUNTU_ECLIPSE_PACKAGES, array(), 'Eclipse and plugins uninstalled successfully.', 'DRUBUNTU_ECLIPSE_UNINSTALL_FAILED', 'Problems were encountered uninstalling Eclipse and plugins.');
    // TODO: Clean up EGit and Drupal Eclipse plugins (in ~/.eclipse/org.eclipse.platform_*/)
  }
  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}