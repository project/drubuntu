<?php

class drubuntu_engine_apache implements drubuntu_engine {
  function install() {
    $hostname = drush_get_option('hostname', 'localhost');

    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('apache-packages', DRUSH_DRUBUNTU_APACHE_PACKAGES);
    drush_drubuntu_exec('Installing Apache packages.', $cmd, array(), 'Apache install successfull.', 'DRUBUNTU_APACHE_INSTALL_FAILED', 'Problems were encountered installing Apache.');

    drush_drubuntu_exec('Adding user to www-data group (to allow access to server created files).', 'sudo adduser $(whoami) www-data', array(), 'User added to www-data group.', 'DRUBUNTU_USER_WWWDATA_GROUP_FAILED', 'Problems were encountered adding the user to www-data group.');
    drush_set_option('new_shell', TRUE);

    // Basic Apache setup.
    drush_drubuntu_exec('Enabling Apache modules.', 'sudo a2enmod ' . drush_get_option('apache-modules', DRUSH_DRUBUNTU_APACHE_MODULES), array(), 'Apache modules enabled successfull.', 'DRUBUNTU_APACHE_MODULES_ENABLE_FAILED', 'Problems were encountered enabling Apache modules.');
    drush_drubuntu_sudo_write('/etc/apache2/httpd.conf', '
# Below configuration added by Drubuntu.
#
# This file configures Apache so it provides access and .htaccess overrides
# to projects in your workspace and enables virtual hosting.

<Directory "' . $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace') .'">
    Options FollowSymLinks
    AllowOverride All

    Order allow,deny
    Allow from all
</Directory>

ServerName '. $hostname);
    drush_drubuntu_exec('Disabling Apache "default" vhost.', 'sudo a2dissite default', array(), 'Apache "default" vhost disabled.', 'DRUBUNTU_APACHE_DEFAULT_VHOST_DISABLE_FAILED', 'Problems were encountered disabling Apache "default" vhost.');

    $workspace = $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace');
    drush_drubuntu_sudo_write('/etc/apache2/sites-available/drubuntu', '
# Below configuration added by Drubuntu.
#
# This file configures Apache so that when you browse to
# http://PLATFORM.' . $hostname . '/ or http://SITE.PLATFORM.' . $hostname . '/ 
# Apache will use the docroot ' . $workspace .'/PLATFORM/.

<VirtualHost *:80>
    ServerName ' . $hostname . '
    ServerAlias *.' . $hostname . '

    # Use VirtualDocumentRoot to serve static files from the correct location.
    # "%-2" means that we ignore the last segment of the hostname - i.e.
    # mysite.myplatform.localhost is mapped to the mysite.myplatform docroot.
    VirtualDocumentRoot ' . $workspace .'/%-2
    <IfModule mod_rewrite.c>
        RewriteEngine On

        # We set the workspace location centrally here to avoid duplication.
        RewriteRule ^.*$ - [E=workspace:' . $workspace .']
        
        # Ensure the hostname is lower case.
        RewriteMap lowercase int:tolower

        # These rewrite rules detect a Drupal installation and add rewrite
        # rules for clean URLs for dynamic vhosts (since the default .htaccess
        # does not work for dynamic vhosts).
        # We use a passthrough (PT) rewrite to work around an Apache issue with
        # SCRIPT_NAME - https://issues.apache.org/bugzilla/show_bug.cgi?id=40102.
        RewriteCond %{HTTP_HOST} ^([^.]*\.)?([^.]+)\.' . $hostname . '
        RewriteCond %{ENV:workspace}/%2/modules/system/system.module -f
        RewriteCond %{ENV:workspace}/%2%{REQUEST_FILENAME} !-f
        RewriteCond %{ENV:workspace}/%2%{REQUEST_FILENAME} !-d
        RewriteRule ^(.*)$ /index.php?q=$1 [QSA,L,PT]
    </IfModule>
</VirtualHost>
');
    drush_drubuntu_exec('Enabling Apache vhost.', 'sudo a2ensite drubuntu', array(), 'Apache vhost enabled.', 'DRUBUNTU_APACHE_VHOST_ENABLE_FAILED', 'Problems were encountered enabling Apache vhost.');

    // User configuration.
    drush_drubuntu_exec('Symlinking the Apache log directory to workspace for easy reference.', 'ln -sfT /var/log/apache2 ' . $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace') .'/logs', array(), 'Apache log directory symlinked to workspace successfully.', 'DRUBUNTU_APACHE_LOG_SYMLINK_FAILED', 'Problems were encountered symlinking the Apache log directory to workspace.');

    drush_drubuntu_exec('Restarting Apache.', 'sudo service apache2 restart', array(), 'Apache restart successfull.', 'DRUBUNTU_APACHE_RESTART_FAILED', 'Problems were encountered restarting Apache.');
  }

  function uninstall($op) {
    drush_drubuntu_exec('Disabling Apache vhost.', 'sudo a2dissite drubuntu', array(), 'Apache vhost disabled.', 'DRUBUNTU_APACHE_VHOST_DISABLE_FAILED', 'Problems were encountered disabling Apache vhost.');
    if ($op == 'purge') {
      drush_drubuntu_exec('Cleaning up Apache vhost.', 'sudo mv /etc/apache2/sites-available/drubuntu ' . drush_drubuntu_backup_dir(), array(), 'Files cleaned up.', 'DRUBUNTU_APACHE_VHOST_CLEANUP_FAILED', 'Problems were encountered cleaning up Apache vhost.');
    }
    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq ' . $op . ' ' . drush_get_option('apache-packages', DRUSH_DRUBUNTU_APACHE_PACKAGES);
    drush_drubuntu_exec('Uninstalling Apache packages.', $cmd, array(), 'Apache uninstall successfull.', 'DRUBUNTU_APACHE_UNINSTALL_FAILED', 'Problems were encountered uninstalling Apache software.');
    if ($op == 'purge') {
      drush_drubuntu_exec('Cleaning up files.', 'sudo mv ' . $_SERVER['HOME'] . '/' . drush_get_option('workspace', 'workspace') . '/logs ' . drush_drubuntu_backup_dir(), array(), 'Files cleaned up.', 'DRUBUNTU_FILE_CLEANUP_FAILED', 'Problems were encountered cleaning up files.');
    }
  }

  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}
